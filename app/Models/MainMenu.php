<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainMenu extends Model
{
    use HasFactory;

    protected $table = 'menus';

    public static function index()
    {
        $menus = Menu::get();

        $menus_arr = [];
        foreach ($menus as $menu) {
            $submenu = SubMenu::where('menu_id', $menu->id)->get();
            // $submenu_arr = [];
            // if ($menu->is_root == 1) {
            //     $submenu = SubMenu::where('menu_id', $menu->id)->get();
            //     foreach ($submenu as $modul) {
            //         $submenu_arr[] = $modul;
            //     }
            // }

            $menus_arr[] = [
                "menu_id" => $menu->id,
                "name" => $menu->name,
                "path" => $menu->path,
                "icon" => $menu->icon,
                "is_root" => $menu->is_root,
                "submenu" => $submenu
            ];
        }

        return $menus_arr;
    }
}
