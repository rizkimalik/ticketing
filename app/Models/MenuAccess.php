<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuAccess extends Model
{
    use HasFactory;

    protected $table = 'menu_access';

    protected $fillable = [
        'access',
        'user_id',
        'user_level',
        'user_create',
        'menu_id',
        'menu_sub_id',
    ];
}
