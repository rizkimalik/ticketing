<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMenu extends Model
{
    use HasFactory;

    protected $table = 'menu_sub';

    protected $fillable = [
        'menu_id',
        'name',
        'number',
        'path',
        'icon',
        'is_root',
    ];
}
