<?php

namespace App\View\Components;

use Illuminate\View\Component;

class GuestMaster extends Component
{
    public $title= 'Ticketing Mendawai';

    public function __construct()
    {
        $this->title = 'Ticketing Mendawai';
    }

    public function render()
    {
        return view('components.app.guest');
    }
}
