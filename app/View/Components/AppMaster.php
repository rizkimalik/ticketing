<?php

namespace App\View\Components;

use App\Models\Menu;
use Illuminate\View\Component;

class AppMaster extends Component
{
    public $title= 'Ticketing Mendawai';
    
    public $style = null;

    public $script = null;

    public function __construct()
    {
        $this->title = 'Ticketing Mendawai';
    }

    public function render()
    {
        $menus = Menu::get();
        
        return view('components.app.master', [
            "menus" => $menus,
        ]);
    }
}
