<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $clients = Client::get();
        // return view('admin.client.client-index', compact('clients'));
        return view('report');
    }
}
