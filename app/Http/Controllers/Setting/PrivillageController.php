<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\MenuAccess;
use App\Models\SubMenu;
use App\Models\UserLevel;
use Illuminate\Http\Request;

class PrivillageController extends Controller
{
    public function index()
    {
        $user_level = UserLevel::get();
        return view('setting.privillage', compact('user_level'));
    }

    public function menu()
    {
        $menu = Menu::select('id', 'name')->get();

        return [
            'status' => 200,
            'message' => 'success',
            'data' => $menu,
        ];
    }

    public function submenu(Request $request)
    {
        $query = $request->all();
        $submenu = SubMenu::select('id', 'menu_id', 'name')->where('menu_id', $query['menu_id'])->get();

        return [
            'status' => 200,
            'message' => 'success',
            'data' => $submenu,
        ];
    }

    public function data_access(Request $request)
    {
        $query = $request->all();
        $menu_access = MenuAccess::where('access', $query['access'])
            ->leftJoin('menu', 'menu.id', '=', 'menu_access.menu_id')
            ->leftJoin('menu_sub', 'menu_sub.id', '=', 'menu_access.menu_sub_id')
            ->where('user_level', $query['user_level'])
            ->select('menu_access.*', 'menu.name AS menu_name', 'menu_sub.name AS menu_subname')
            ->orderBy('menu_id','asc')
            ->orderBy('menu_sub_id','asc')
            ->get();

        return [
            'status' => 200,
            'message' => 'success',
            'data' => $menu_access,
            'totalCount' => $menu_access->count()
        ];
    }

    public function insert_access(Request $request)
    {
        $check = MenuAccess::where('user_level', $request->user_level)
            ->where('menu_id', $request->menu_id)
            ->count();

        if ($check > 0) {
            return [
                'status' => 200,
                'message' => 'success',
                'data' => 'Data Already exists.',
            ];
        } else {
            MenuAccess::create([
                'access' => $request->access,
                'user_id' => 0,
                'user_level' => $request->user_level,
                'user_create' => $request->user_create,
                'menu_id' => $request->menu_id,
                'menu_sub_id' => $request->menu_sub_id,
            ]);

            return [
                'status' => 200,
                'message' => 'success',
                'data' => 'Insert Data Success.',
            ];
        }
    }

    public function destroy_access(Request $request)
    {
        //! pending delete filter by menu / sub id
        MenuAccess::where('id', $request->id)->delete();

        return [
            'status' => 200,
            'message' => 'success',
            'data' => 'Delete Data Success.',
        ];
    }
}
