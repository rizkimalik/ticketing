<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(Request $request)
    {
        return Validator::make($request, [
            'name' => ['required', 'string', 'max:100'],
            'username' => ['required', 'string', 'max:100'],
            'email_address' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'company' => ['required','string', 'max:100'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    public function create()
    {
        return view('auth.register');
    }

    protected function store(Request $request)
    {
        User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email_address' => $request->email,
            'password' => Hash::make($request->password),
            'company' => $request->company,
            // 'user_level' => $request->user_level,
            'user_level' => 'Administrator',
            'login' => '0',
            'organization' => '1',
            'department' => '1',
        ]);

        return redirect('/register')->with('status', 'Register Successfully.');
    }
}
