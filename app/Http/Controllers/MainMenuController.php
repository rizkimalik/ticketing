<?php

namespace App\Http\Controllers;

use App\Models\MainMenu;
use App\Models\Menu;
use App\Models\MenuAccess;
use App\Models\SubMenu;
use Illuminate\Http\Request;

class MainMenuController extends Controller
{
    public function index()
    {
        $mainmenu = MainMenu::index();
        
        return $mainmenu;
    }

    public function menu()
    {
        $menu = Menu::select('id','name')->get();
        
        return [
            'status' => 200,
            'message' => 'success',
            'data' => $menu, 
        ];
    }

    public function submenu(Request $request)
    {
        $query = $request->all();
        $submenu = SubMenu::select('id','menu_id','name')->where('menu_id', $query['menu_id'])->get();
        
        return [
            'status' => 200,
            'message' => 'success',
            'data' => $submenu, 
        ];
    }

    public function insert_access(Request $request)
    {
        $menu_access = MenuAccess::create([
            'company_name' => $request->company_name,
            'company_telp' => $request->company_telp,
            'company_address' => $request->company_address,
        ]);

        return [
            'status' => 200,
            'message' => 'success',
            'data' => $menu_access, 
        ];
    }
}
