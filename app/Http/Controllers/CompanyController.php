<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        // $clients = Client::get();
        // return view('admin.client.client-index', compact('clients'));
        return view('company.company-list');
    }

    public function data(Request $request)
    {
        $query = $request->all();
        $skip = isset($query['skip']) ? $query['skip'] : 0;
        $take = isset($query['take']) ? $query['take'] : 10;

        if (isset($query['sort'])) {
            $sort_decode = json_decode($query['sort']);
            $sort = $sort_decode[0];
            $selector = $sort->selector;
            $order = $sort->desc == true ? 'desc' : 'asc';
        }
        else {
            $selector = 'id';
            $order = 'asc';
        }

        if (isset($query['filter'])) {
            $filter = json_decode($query['filter']);
            $key = head($filter);
            $value = last($filter);

            $companies = Company::where($key, 'LIKE', '%'.$value.'%')
                                    ->orderBy($selector, $order)
                                    ->skip($skip)
                                    ->take($take)
                                    ->get();
            $total = Company::where($key, 'LIKE', '%'.$value.'%')->count();

        }
        else {
            $companies = Company::orderBy($selector, $order)->skip($skip)->take($take)->get();
            $total = Company::count();
        }

        return [
            'data' => $companies, 
            'totalCount' => $total
        ];
    }

    public function store(Request $request)
    {
        $request->validate([
            'company_name' => 'required',
            'company_telp' => 'required',
            'company_address' => 'required'
        ]);

        $company = Company::create([
            'company_name' => $request->company_name,
            'company_telp' => $request->company_telp,
            'company_address' => $request->company_address,
        ]);

        return [
            'status' => 200,
            'message' => 'success',
            'data' => $company, 
        ];
    }

    public function update(Request $request, Company $company)
    {
        $input = $request->all();

        //? get data by ID
        $data = Company::findOrFail($company->id);

        $data->update([
            'company_name' => isset($input['company_name']) ? $input['company_name'] : $data['company_name'],
            'company_telp' => isset($input['company_telp']) ? $input['company_telp'] : $data['company_telp'],
            'company_address' => isset($input['company_address']) ? $input['company_address'] : $data['company_address'],
        ]);
        
        return [
            'status' => 200,
            'message' => 'success',
            'data' => $data, 
        ];
    }

    public function destroy(Company $company)
    {
        $data = Company::where('id', $company->id)->delete();

        return [
            'status' => 200,
            'message' => 'success',
            'data' => $data, 
        ];
    }
}
