<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Setting\PrivillageController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', [UserController::class, 'index']);
Route::get('/user', [UserController::class, 'index']);
Route::get('/menu', [PrivillageController::class, 'menu']);
Route::get('/submenu/{menu_id}', [PrivillageController::class, 'submenu']);
Route::get('/data_access', [PrivillageController::class, 'data_access']);
Route::post('/insert_access', [PrivillageController::class, 'insert_access']);
Route::delete('/destroy_access/{id}', [PrivillageController::class, 'destroy_access']);

Route::get('/company/data', [CompanyController::class, 'data']);
Route::post('/company/store', [CompanyController::class, 'store']);
Route::put('/company/update/{company:id}', [CompanyController::class, 'update']);
Route::delete('/company/destroy/{company:id}', [CompanyController::class, 'destroy']);


