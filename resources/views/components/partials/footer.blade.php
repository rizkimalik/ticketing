<footer class="footer py-4 d-flex flex-lg-column border-top" id="kt_footer">
    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted fw-bold me-1">2022</span>
            <span target="_blank" class="text-gray-800 text-hover-primary">VELOS APL</span>
        </div>
    </div>
</footer>