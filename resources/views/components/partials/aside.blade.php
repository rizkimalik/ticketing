<aside id="kt_aside" class="aside aside-light aside-hoverable " data-kt-drawer="true"
    data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
    data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start"
    data-kt-drawer-toggle="#kt_aside_mobile_toggle">

    <div class="aside-logo flex-column-auto" id="kt_aside_logo">
        <img alt="Logo" src="{{ asset('theme/media/logo-hitam-50.png') }}" class="h-40px logo" />
        
        <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle"
            data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
            data-kt-toggle-name="aside-minimize">
            <span class="svg-icon svg-icon-1 rotate-180">
                <x-icons.arrows />
            </span>
        </div>
    </div>
    <div class="separator separator-dashed"></div>

    <div class="aside-menu flex-column-fluid">
        <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
            data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
            data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu"
            data-kt-scroll-offset="0">
            <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true" data-kt-menu-expand="false">
                <div class="menu-item">
                    <div class="menu-content pb-2">
                        <span class="menu-section text-muted text-uppercase fs-8 ls-1">Main Menu</span>
                    </div>
                </div>
                @php
                    // $segment = request()->segments();
                    // $uri = Route::current()->uri;   
                @endphp
                @foreach ($menus as $menu)
                    @if ($menu->is_root == 1)
                        {{-- <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ ($segment[0] == str($menu->name)->lower()) ? 'here show' : '' }}"> --}}
                        <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                            <span class="menu-link">
                                <span class="menu-icon">
                                    <i class="{{ $menu->icon }}"></i>
                                </span>
                                <span class="menu-title">{{ $menu->name }}</span>
                                <span class="menu-arrow"></span>
                            </span>
                            <div class="menu-sub menu-sub-accordion">
                                @php
                                    $submenu = App\Models\SubMenu::where('menu_id', $menu->id)->select('name','path','menu_id','icon')->get();
                                @endphp

                                @foreach ($submenu as $sub)
                                    {{-- @php
                                        $segment_submenu = "";
                                        if (isset($segment[1])) {
                                            $segment_submenu = $segment[1];
                                        }
                                    @endphp --}}

                                    <div class="menu-item">
                                        {{-- <a class="menu-link {{ ($segment_submenu == substr($sub->path, 1)) ? 'active' : '' }}" href="{{ $sub->path }}"> --}}
                                        <a class="menu-link {{ (request()->is(substr($sub->path, 1))) ? 'active' : '' }}" href="{{ $sub->path }}">
                                            {{-- <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span> --}}
                                            <span class="menu-icon">
                                                <i class="{{ $sub->icon }}"></i>
                                            </span>
                                            <span class="menu-title">{{ $sub->name }}</span>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <div class="menu-item menu-active-bg">
                            <a class="menu-link {{ (request()->is(substr($menu->path, 1))) ? 'active' : '' }}" href="{{ $menu->path }}">
                                <span class="menu-icon">
                                    <i class="{{ $menu->icon }}"></i>
                                </span>
                                <span class="menu-title">{{ $menu->name }}</span>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

</aside>