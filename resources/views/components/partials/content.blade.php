<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                {{ $slot }}
            </div>
        </div>
    </div>
</div>