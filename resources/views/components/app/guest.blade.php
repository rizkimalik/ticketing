<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>

    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" />

    {{-- Stylesheets --}}
    <link rel="stylesheet" href="{{ asset('theme/plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/style.bundle.css') }}">
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed">

    <div class="d-flex flex-column flex-root">
        {{ $slot }}
    </div>

    {{-- Javascript --}}
    <script src="{{ asset('theme/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('theme/js/scripts.bundle.js') }}"></script>
</body>

</html>