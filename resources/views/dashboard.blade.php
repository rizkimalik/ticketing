<x-app-master>
    <x-slot:style>
        <link href="{{ asset('theme/plugins/devex/dx.light.css') }}" rel="stylesheet" />
    </x-slot:style>

    <x-partials.toolbar title="Dashboard" subtitle="" />
    <x-partials.content>
        <div class="row mb-5">
            <div class="col-xl-3">
                <div class="card card-custom card-stretch gutter-b border">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-50px symbol-circle">
                                <span class="symbol-label bg-primary">
                                    <i class="fas fa-home text-light"></i>
                                </span>
                            </div>
                            <div class="flex-grow-1 mx-2">
                                <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Total Driver</a>
                                <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                            </div>
                            <span class="fw-bolder text-success fs-3">50</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="card card-custom card-stretch gutter-b border">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-50px symbol-circle">
                                <span class="symbol-label bg-primary">
                                    <i class="fas fa-users text-light"></i>
                                </span>
                            </div>
                            <div class="flex-grow-1 mx-2">
                                <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Total Customer</a>
                                <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                            </div>
                            <span class="fw-bolder text-success fs-3">50</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="card card-custom card-stretch gutter-b border">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-50px symbol-circle">
                                <span class="symbol-label bg-primary">
                                    <i class="fas fa-building text-light"></i>
                                </span>
                            </div>
                            <div class="flex-grow-1 mx-2">
                                <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Total Company</a>
                                <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                            </div>
                            <span class="fw-bolder text-success fs-3">50</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="card card-custom card-stretch gutter-b border">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-50px symbol-circle">
                                <span class="symbol-label bg-primary">
                                    <i class="fas fa-car text-light"></i>
                                </span>
                            </div>
                            <div class="flex-grow-1 mx-2">
                                <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Total Parking</a>
                                <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                            </div>
                            <span class="fw-bolder text-success fs-3">50</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-xl-8">
                <div class="card border">
                    <div class="card-header">
                        <div class="card-title">Sales Report</div>
                    </div>
                    <div class="card-body">
                        <div class="h-300px" id="DashSalesReport"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card border">
                    <div class="card-header">
                        <div class="card-title">Sales brackdown by Product</div>
                    </div>
                    <div class="card-body">
                        <div class="h-300px" id="DashSalesProduct"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-xl-12">
                <div class="card border">
                    <div class="card-header">
                        <div class="card-title">Analytics Traffic</div>
                    </div>
                    <div class="card-body">
                        <div class="h-300px" id="DashGridData"></div>
                    </div>
                </div>
            </div>
        </div>
    </x-partials.content>

    <x-slot:script>
        <script src="{{ asset('theme/plugins/devex/dx.all.js') }}"></script>
        <script src="{{ asset('theme/plugins/echarts.min.js') }}"></script>
        <script>
            function DashSalesReport() {
                var chartDom = document.getElementById('DashSalesReport');
                var myChart = echarts.init(chartDom);
                var option;

                option = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'cross',
                            crossStyle: {
                                color: '#999'
                            }
                        }
                    },
                    toolbox: {
                        feature: {
                            dataView: { show: true, readOnly: false },
                            magicType: { show: true, type: ['line', 'bar'] },
                            restore: { show: true },
                            saveAsImage: { show: true }
                        }
                    },
                    legend: {
                        data: ['Evaporation', 'Precipitation', 'Temperature']
                    },
                    xAxis: [
                        {
                            type: 'category',
                            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                            axisPointer: {
                                type: 'shadow'
                            }
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            name: 'Precipitation',
                            min: 0,
                            max: 250,
                            interval: 50,
                            axisLabel: {
                                formatter: '{value} ml'
                            }
                        },
                        {
                            type: 'value',
                            name: 'Temperature',
                            min: 0,
                            max: 25,
                            interval: 5,
                            axisLabel: {
                                formatter: '{value} °C'
                            }
                        }
                    ],
                    series: [
                        {
                            name: 'Evaporation',
                            type: 'bar',
                            tooltip: {
                                valueFormatter: function (value) {
                                    return value + ' ml';
                                }
                            },
                            data: [
                                2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3
                            ]
                        },
                        {
                            name: 'Precipitation',
                            type: 'bar',
                            tooltip: {
                                valueFormatter: function (value) {
                                    return value + ' ml';
                                }
                            },
                            data: [
                                2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3
                            ]
                        },
                        {
                            name: 'Temperature',
                            type: 'line',
                            yAxisIndex: 1,
                            tooltip: {
                                valueFormatter: function (value) {
                                    return value + ' °C';
                                }
                            },
                            data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
                        }
                    ]
                };

                option && myChart.setOption(option);

            }
            DashSalesReport();


            function DashSalesProduct() {
                var chartDom = document.getElementById('DashSalesProduct');
                var myChart = echarts.init(chartDom);
                var option = {
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        top: '5%',
                        left: 'center'
                    },
                    series: [
                        {
                            name: 'Access From',
                            type: 'pie',
                            radius: ['40%', '70%'],
                            avoidLabelOverlap: false,
                            itemStyle: {
                                borderRadius: 10,
                                borderColor: '#fff',
                                borderWidth: 2
                            },
                            label: {
                                show: true,
                                position: 'middle',
                                formatter: '{b}: {c}'
                            },
                            emphasis: {
                                label: {
                                    show: false,
                                }
                            },
                            labelLine: {
                                show: false
                            },
                            data: [
                                { value: 735, name: 'Direct' },
                                { value: 580, name: 'Email' },
                                { value: 484, name: 'Union Ads' },
                                { value: 300, name: 'Video Ads' }
                            ]
                        }
                    ]
                }

                option && myChart.setOption(option);
            }
            DashSalesProduct();

            function DashGridData(){
                const customers = [{
                    ID: 1,
                    CompanyName: 'Super Mart of the West',
                    Address: '702 SW 8th Street',
                    City: 'Bentonville',
                    State: 'Arkansas',
                    Zipcode: 72716,
                    Phone: '(800) 555-2797',
                    Fax: '(800) 555-2171',
                    Website: 'http://www.nowebsitesupermart.com',
                }, {
                    ID: 2,
                    CompanyName: 'Electronics Depot',
                    Address: '2455 Paces Ferry Road NW',
                    City: 'Atlanta',
                    State: 'Georgia',
                    Zipcode: 30339,
                    Phone: '(800) 595-3232',
                    Fax: '(800) 595-3231',
                    Website: 'http://www.nowebsitedepot.com',
                }];

                $('#DashGridData').dxDataGrid({
                    dataSource: customers,
                    keyExpr: 'ID',
                    columns: ['CompanyName', 'City', 'State', 'Phone', 'Fax'],
                    showBorders: true,
                });
            }
            DashGridData();
        </script>
    </x-slot:script>
</x-app-master>