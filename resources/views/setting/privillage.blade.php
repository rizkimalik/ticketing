<x-app-master>
    <x-slot:style>
        <link href="{{ asset('theme/plugins/devex/dx.light.css') }}" rel="stylesheet" />
    </x-slot:style>

    <x-partials.toolbar title="Setting" subtitle="User Privillage" />
    <x-partials.content>
        <div class="row mb-5">
            <div class="col-xl-12">
                <div class="card border">
                    <div class="card-header">
                        <div class="card-title">User Privillage</div>
                    </div>
                    <div class="card-body">
                        <div id="dxGridUserLevel"></div>
                    </div>
                </div>
            </div>
        </div>
    </x-partials.content>

    @php
        $user_create = Auth::user()->username;
    @endphp

    <x-slot:script>
        <script src="{{ asset('theme/plugins/devex/dx.all.js') }}"></script>
        <script>
            function GridDataUserLevel() {
                const user_level = @json($user_level);

                $('#dxGridUserLevel').dxDataGrid({
                    dataSource: user_level,
                    remoteOperations: true,
                    paging: {
                        pageSize: 10,
                    },
                    pager: {
                        visible: true,
                        allowedPageSizes: [10, 20, 50],
                        showPageSizeSelector: true,
                        showInfo: true,
                        showNavigationButtons: true,
                    },
                    allowColumnResizing: true,
                    columnMinWidth: 100,
                    showBorders: true,
                    showRowLines: true,
                    showColumnLines: true,
                    hoverStateEnabled: true,
                    filterRow: {
                        visible: true,
                    },
                    keyExpr: "id",
                    columns: [{
                        caption: 'Level Name',
                        dataField: 'level_name',
                    }, {
                        caption: 'Description',
                        dataField: 'description',
                    }],
                    masterDetail: {
                        enabled: true,
                        template: MenuDetail,
                    },
                });

                function MenuDetail(container, options) {
                    const data = options.data;

                    const store_access = new DevExpress.data.CustomStore({
                        key: 'id',
                        load(options) {
                            const deferred = $.Deferred();
                            $.ajax({
                                url: `/api/data_access?access=menu&user_level=${data.level_name}`,
                                dataType: 'json',
                                method: 'GET',
                                cache: false,
                                async: true,
                                success(result) {
                                    deferred.resolve(result.data, {
                                        totalCount: result.totalCount,
                                    });
                                },
                                error() {
                                    deferred.reject('Data Loading Error');
                                },
                            });
                            return deferred.promise();
                        },
                        remove(key) {
                            $.ajax({
                                url: `/api/destroy_access/${key}`,
                                method: 'DELETE',
                                data: { id: key },
                                success(result) {
                                    if (result.status === 200) {
                                        DevExpress.ui.notify({
                                            message: result.data,
                                            height: 50,
                                            width: 300,
                                            position: 'top right',
                                        }, 'success', 3000);
                                    } else {
                                        DevExpress.ui.notify({
                                            message: 'Delete Data Error.',
                                            height: 50,
                                            width: 300,
                                            position: 'top right',
                                        }, 'error', 3000);
                                    }
                                },
                                error() {
                                    console.error('Data Loading Error');
                                    alert('Data error.')
                                },
                                timeout: 5000,
                            });
                        },
                    });

                    const store_menu = new DevExpress.data.CustomStore({
                        key: 'id',
                        load(options) {
                            const deferred = $.Deferred();
                            $.ajax({
                                url: `/api/menu`,
                                dataType: 'json',
                                method: 'GET',
                                cache: false,
                                async: true,
                                success(result) {
                                    deferred.resolve(result.data);
                                },
                                error() {
                                    deferred.reject('Data Loading Error');
                                },
                            });
                            return deferred.promise();
                        },
                    });

                    container.html(`
                        <form action="#" id="form-${data.id}" class="mt-0 mb-5">
                            <div id="content-${data.id}"></div>
                        </form>
                        <div id="dxGridDataAccess-${data.id}"></div>
                    `);

                    const dxFormMenu = $(`#content-${data.id}`).dxForm({
                        readOnly: false,
                        colCount: 2,
                        items: [{
                            itemType: 'group',
                            items: [{
                                dataField: `menu${data.id}`,
                                editorType: 'dxSelectBox',
                                editorOptions: {
                                    text: 'Menu',
                                    dataSource: store_menu,
                                    valueExpr: 'id',
                                    displayExpr: 'name',
                                    deferRendering: false,
                                    searchEnabled: true,
                                },
                                validationRules: [{
                                    type: 'required',
                                    message: 'Menu is required',
                                }],
                            }],
                        },{
                            itemType: 'group',
                            items: [{
                                itemType: 'button',
                                horizontalAlignment: 'left',
                                buttonOptions: {
                                    text: 'Submit',
                                    useSubmitBehavior: true,
                                }
                            }],
                        }],
                    });

                    $(`#form-${data.id}`).on('submit', (e) => {
                        e.preventDefault();
                        
                        $.ajax({
                            url: `/api/insert_access`,
                            method: 'POST',
                            data: {
                                access: 'menu',
                                user_create: @json($user_create),
                                user_level: data.level_name,
                                menu_id: $(`[name=menu${data.id}]`).val(),
                                menu_sub_id: '0',
                            },
                            success(result) {
                                if (result.status === 200) {
                                    const GridDataAccess = $(`#dxGridDataAccess-${data.id}`).dxDataGrid("instance");
                                    GridDataAccess.refresh();

                                    DevExpress.ui.notify({
                                        message: result.data,
                                        height: 50,
                                        width: 300,
                                        position: 'top right',
                                    }, 'success', 3000);
                                } else {
                                    DevExpress.ui.notify({
                                        message: 'Insert Data Error.',
                                        height: 50,
                                        width: 300,
                                        position: 'top right',
                                    }, 'error', 3000);
                                }
                            },
                            error(error) {
                                console.error(error);
                                DevExpress.ui.notify({
                                    message: 'Insert Data Error.',
                                    height: 50,
                                    width: 300,
                                    position: 'top right',
                                }, 'error', 3000);
                            },
                            timeout: 5000,
                        });

                    });

                    $(`#dxGridDataAccess-${data.id}`).dxDataGrid({
                        dataSource: store_access,
                        remoteOperations: true,
                        paging: {
                            pageSize: 10,
                        },
                        pager: {
                            visible: true,
                            allowedPageSizes: [10, 20],
                            showPageSizeSelector: true,
                            showInfo: true,
                            showNavigationButtons: true,
                        },
                        showBorders: true,
                        showRowLines: true,
                        showColumnLines: true,
                        hoverStateEnabled: true,
                        editing: {
                            mode: 'row',
                            allowDeleting: true,
                        },
                        masterDetail: {
                            enabled: true,
                        },
                        columns: [{
                            caption: 'Menu',
                            dataField: 'menu_name',
                        }, {
                            type: 'buttons',
                            caption: 'Actions',
                            buttons: [{
                                hint: 'Delete Data',
                                icon: 'trash',
                                name: 'delete',
                                cssClass: "text-danger"
                            }],
                        }],
                    });
                }
            }
            GridDataUserLevel();

            
        </script>
    </x-slot:script>
</x-app-master>