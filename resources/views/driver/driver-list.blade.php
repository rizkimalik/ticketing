<x-app-master>
    <x-partials.toolbar title="Driver" subtitle="List Driver" />

    <x-partials.content>
        <div class="row mb-5">
            <div class="col-xl-12">
                <div class="card border">
                    <div class="card-body d-flex flex-column flex-center">
                        <div class="mb-2">
                            <h1 class="fw-bold text-gray-800 text-center lh-lg">Driver</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </x-partials.content>
</x-app-master>