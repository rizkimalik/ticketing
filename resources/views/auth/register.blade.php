<x-guest-master>
    <div class="d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat"
            style="background-image: url({{ asset('theme/media/background.jpg') }});">

            <div class="login-form text-center p-7 position-relative overflow-hidden col-lg-3">
                <div class="d-flex flex-center mb-15">
                    <a href="#">
                        <img src="{{ asset('theme/media/mendawai.png') }}" class="h-50px" alt="logo">
                    </a>
                </div>
                <div class="login-signin">
                    <div class="mb-10">
                        <h3>Sign Up to Application</h3>
                    </div>

                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {{ session('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif

                    <form method="POST" action="{{ url('register') }}">
                        @csrf

                        {{-- <div class="form-group mb-5 fv-plugins-icon-container">
                            <input type="text" name="company" class="form-control" placeholder="Company" required>
                        </div> --}}
                        <div class="form-group mb-5 fv-plugins-icon-container">
                            <input type="text" name="name" class="form-control" placeholder="Full Name" required>
                        </div>
                        <div class="form-group mb-5 fv-plugins-icon-container">
                            <input type="text" name="username"
                                class="form-control  @error('username') is-invalid @enderror" 
                                placeholder="Username"
                                autocomplete="username" required>
                            <div class="fv-plugins-message-container"></div>
                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group mb-5 fv-plugins-icon-container">
                            <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror"
                                placeholder="Email" autocomplete="email" required>
                            <div class="fv-plugins-message-container"></div>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group mb-5 fv-plugins-icon-container">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>

                        <a href="{{ url('/login') }}" class="btn btn-light-primary font-weight-bold my-5 mx-10">Sign In</a>
                        <button type="submit" class="btn btn-primary font-weight-bold my-5 mx-10">Sign Up</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-guest-master>