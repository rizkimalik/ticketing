<x-guest-master>
    <div class="d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat"
            style="background-image: url({{ asset('theme/media/background.jpg') }});">

            <div class="login-form text-center p-7 position-relative overflow-hidden col-md-3 col-sm-2">
                <div class="d-flex flex-center mb-15">
                    <a href="#">
                        <img src="{{ asset('theme/media/logo-hitam-50.png') }}" class="h-40px" alt="logo">
                    </a>
                </div>
                <div class="login-signin">
                    <div class="mb-20">
                        <h3>Sign In to Application</h3>
                        <div class="text-muted font-weight-bold">Enter your details to login to your account:</div>
                    </div>
                    
                    @error('username')
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        {{ $message }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @enderror

                    <form method="POST" action="{{ url('login') }}">
                        @csrf

                        <div class="form-group mb-5">
                            <input type="text" name="username" class="form-control" 
                                placeholder="Username" autocomplete="username" required>
                        </div>
                        <div class="form-group mb-5">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                            <div class="checkbox-inline">
                                <label class="checkbox m-0 text-muted">
                                    <input type="checkbox" name="remember">
                                    <span></span>Remember me</label>
                            </div>
                            <a href="#" class="text-muted text-hover-primary">Forget Password ?</a>
                        </div>
                        <button type="submit" class="btn btn-primary font-weight-bold my-5">Sign In</button>
                        <input type="hidden">
                        <div></div>
                    </form>
                    <div class="mt-10">
                        <span class="opacity-70 mr-4">Don't have an account yet?</span>
                        <a href="{{ url('/register') }}" class="text-muted text-hover-primary font-weight-bold">Sign Up!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest-master>