<x-app-master>
    <x-slot:style>
        <link href="{{ asset('theme/plugins/devex/dx.light.css') }}" rel="stylesheet" />
    </x-slot:style>

    <x-partials.toolbar title="Company" subtitle="List Company" />
    <x-partials.content>
        <div class="row mb-5">
            <div class="col-xl-12">
                <div class="card border">
                    {{-- <div class="card-header">
                        <div class="card-title">Data Company</div>
                    </div> --}}
                    <div class="card-body">
                        <div id="dxGridCompany"></div>
                    </div>
                </div>
            </div>
        </div>
    </x-partials.content>

    <x-slot:script>
        <script src="{{ asset('theme/plugins/devex/dx.all.js') }}"></script>
        <script>
            function GridDataCompany() {
                function isNotEmpty(value) {
                    return value !== undefined && value !== null && value !== '';
                }

                const store_company = new DevExpress.data.CustomStore({
                    key: 'id',
                    load(options) {
                        const deferred = $.Deferred();
                        const args = {};
                        [
                            'skip',
                            'take',
                            'requireTotalCount',
                            'sort',
                            'filter',
                        ].forEach((i) => {
                            if (i in options && isNotEmpty(options[i])) {
                                args[i] = JSON.stringify(options[i]);
                            }
                        });

                        $.ajax({
                            url: `/api/company/data`,
                            dataType: 'json',
                            method: 'GET',
                            cache: false,
                            async: true,
                            data: args,
                            success(result) {
                                deferred.resolve(result.data, {
                                    totalCount: result.totalCount,
                                });
                            },
                            error() {
                                deferred.reject('Data Loading Error');
                            },
                        });

                        return deferred.promise();
                    },
                    insert(values) {
                        $.ajax({
                            url: `/api/company/store`,
                            method: 'POST',
                            data: values,
                            success(result) {
                                if (result.status === 200) {
                                    alert('Insert Data Success.')
                                } else {
                                    alert(result.data)
                                }
                            },
                            error(error) {
                                console.error(error);
                                alert('Data error.')
                            },
                            timeout: 5000,
                        });
                    },
                    update(key, values) {
                        $.ajax({
                            url: `/api/company/update/${key}`,
                            method: 'PUT',
                            data: values,
                            success(result) {
                                if (result.status === 200) {
                                    alert('Update Data Success.')
                                } else {
                                    alert(result.data)
                                }
                            },
                            error(error) {
                                console.error(error);
                                alert('Data error.')
                            },
                            timeout: 5000,
                        });
                    },
                    remove(key) {
                        $.ajax({
                            url: `/api/company/destroy/${key}`,
                            method: 'DELETE',
                            data: { id: key },
                            success(result) {
                                if (result.status === 200) {
                                    alert('Delete Data Success.')
                                } else {
                                    alert(result.data)
                                }
                            },
                            error() {
                                console.error('Data Loading Error');
                                alert('Data error.')
                            },
                            timeout: 5000,
                        });
                    },
                });

                $('#dxGridCompany').dxDataGrid({
                    dataSource: store_company,
                    remoteOperations: true,
                    paging: {
                        pageSize: 10,
                    },
                    pager: {
                        visible: true,
                        allowedPageSizes: [10, 20, 50],
                        showPageSizeSelector: true,
                        showInfo: true,
                        showNavigationButtons: true,
                    },
                    allowColumnResizing: true,
                    columnMinWidth: 100,
                    showBorders: true,
                    showRowLines: true,
                    showColumnLines: true,
                    hoverStateEnabled: true,
                    filterRow: {
                        visible: true,
                    },
                    editing: {
                        mode: 'row',
                        allowUpdating: true,
                        allowDeleting: true,
                        allowAdding: true,
                    },
                    onToolbarPreparing: function(e) {
                        e.toolbarOptions.items[0].showText = "always";
                        e.toolbarOptions.items[0].options.text = "Create New";
                        e.toolbarOptions.items[0].options.icon = "plus";
                        e.toolbarOptions.items[0].options.hint = "Create New Data";
                    },
                    columns: [{
                        type: 'buttons',
                        caption: 'Actions',
                        buttons: [{
                            hint: 'Update Data',
                            icon: 'edit',
                            name: 'edit',
                            cssClass: "text-warning"
                        }, {
                            hint: 'Delete Data',
                            icon: 'trash',
                            name: 'delete',
                            cssClass: "text-danger"
                        }],
                    }, {
                        caption: 'Company Name',
                        dataField: 'company_name',
                    }, {
                        caption: 'Telphone',
                        dataField: 'company_telp',
                    }, {
                        caption: 'Address',
                        dataField: 'company_address',
                    }],
                });
            }
            GridDataCompany();
        </script>
    </x-slot:script>
</x-app-master>