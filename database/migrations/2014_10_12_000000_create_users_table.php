<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('username', 100)->unique();
            $table->string('email_address', 100)->unique();
            $table->string('password', 100);
            $table->string('company', 100)->nullable();
            $table->string('user_level', 100)->nullable();
            $table->integer('login')->nullable();
            $table->integer('aux')->nullable();
            $table->integer('organization')->nullable();
            $table->integer('department')->nullable();

            $table->boolean('inbound');
            $table->boolean('outbound');
            $table->boolean('sms');
            $table->boolean('email');
            $table->boolean('chat');
            $table->boolean('facebook');
            $table->boolean('twitter');
            $table->boolean('instagram');
            $table->boolean('whatsapp');
            $table->integer('max_email');
            $table->integer('max_chat');
            $table->integer('max_queue');
            $table->integer('max_concurrent');
            $table->integer('socket_id');

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
