<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('menu_access', function (Blueprint $table) {
            $table->id();
            $table->string('access', 20);
            $table->integer('user_id');
            $table->string('user_level', 20);
            $table->string('user_create', 50);
            $table->integer('menu_id');
            $table->integer('menu_sub_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('menu_access');
    }
};
