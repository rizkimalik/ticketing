<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenuSeeder extends Seeder
{

    public function run()
    {
        // Menu::factory(1)->create();
        // Menu::factory()->create([
        Menu::insert([
            [
                'name' => 'Dashboard',
                'number' => 1,
                'path' => '/dashboard',
                'icon' => 'fas fa-desktop',
                'is_root' => 0,
            ],[
                'name' => 'Master',
                'number' => 2,
                'path' => '',
                'icon' => 'fas fa-ruler-combined',
                'is_root' => 1,
            ],[
                'name' => 'Customers',
                'number' => 4,
                'path' => '/customer',
                'icon' => 'fas fa-adress-card',
                'is_root' => 0,
            ],[
                'name' => 'Todolist',
                'number' => 5,
                'path' => '/todolist',
                'icon' => 'fas fa-clipboard-list',
                'is_root' => 0,
            ],[
                'name' => 'Channels',
                'number' => 5,
                'path' => '',
                'icon' => 'fas fa-project-diagram',
                'is_root' => 1,
            ],[
                'name' => 'Ticket',
                'number' => 5,
                'path' => '',
                'icon' => 'fas fa-ticket-lat',
                'is_root' => 1,
            ],[
                'name' => 'Report',
                'number' => 3,
                'path' => '',
                'icon' => 'fas fa-chart-bar',
                'is_root' => 1,
            ],[
                'name' => 'Settings',
                'number' => 6,
                'path' => '',
                'icon' => 'fas fa-sliders-h',
                'is_root' => 1,
            ],
        ]);
    }
}
