<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\UserLevel;

class UserLevelSeeder extends Seeder
{

    public function run()
    {
        UserLevel::insert([
            ['level_name' => 'Layer1', 'description' => 'Level CS Agent'],
            ['level_name' => 'Layer2', 'description' => 'Level CS Team Leader'],
            ['level_name' => 'Layer3', 'description' => 'Level Case Unit / PIC'],
            ['level_name' => 'Supervisor', 'description' => 'Level Supervisor'],
            ['level_name' => 'Administrator', 'description' => 'Level Administrator'],
        ]);
    }
}
