<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SubMenu;

class SubMenuSeeder extends Seeder
{

    public function run()
    {
        SubMenu::insert([
            // menu dashboard
            ['menu_id' => 1, 'name' => 'Ticket', 'number' => 0, 'path' => '/dashboard_ticket', 'icon' => 'far fa-analytics', 'is_root' => 0],
            ['menu_id' => 1, 'name' => 'Social Media', 'number' => 0, 'path' => '/dashboard_socmed', 'icon' => 'far fa-comments-alt', 'is_root' => 0],
            ['menu_id' => 1, 'name' => 'Sentiment', 'number' => 0, 'path' => '/dashboard_sentiment', 'icon' => 'far fa-analytics', 'is_root' => 0],
            
            // menu master            
            ['menu_id' => 2, 'name' => 'Category', 'number' => 0, 'path' => '/category', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 2, 'name' => 'Category Produk', 'number' => 0, 'path' => '/categorysublv1', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 2, 'name' => 'Category Case', 'number' => 0, 'path' => '/categorysublv2', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 2, 'name' => 'Category Detail', 'number' => 0, 'path' => '/categorysublv3', 'icon' => 'far fa-dot-circle', 'is_root' => 0],

            //menu channels
            ['menu_id' => 5, 'name' => 'Voice', 'number' => 0, 'path' => '/channel_voice', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 5, 'name' => 'Email', 'number' => 0, 'path' => '/channel_email', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 5, 'name' => 'Social Media', 'number' => 0, 'path' => '/channel_socmed', 'icon' => 'far fa-dot-circle', 'is_root' => 0],

            // menu ticket
            ['menu_id' => 6, 'name' => 'Create Ticket', 'number' => 0, 'path' => '/ticket_create', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 6, 'name' => 'History Ticket', 'number' => 0, 'path' => '/ticket_history', 'icon' => 'far fa-dot-circle', 'is_root' => 0],

            //menu Report
            ['menu_id' => 7, 'name' => 'Report Ticket', 'number' => 0, 'path' => '/report_ticket', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 7, 'name' => 'Report Email', 'number' => 0, 'path' => '/report_email', 'icon' => 'far fa-dot-circle', 'is_root' => 0],

            // menu settings
            ['menu_id' => 8, 'name' => 'User Management', 'number' => 0, 'path' => '/setting_user', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
            ['menu_id' => 8, 'name' => 'User Privillages', 'number' => 0, 'path' => '/setting_privillage', 'icon' => 'far fa-dot-circle', 'is_root' => 0],
        ]);
    }
}
